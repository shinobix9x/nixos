{ config, pkgs, ... }:

{
  imports = [
    ./apps/starship.nix
    ./apps/fish.nix
    ./apps/neovim.nix
    ./apps/git.nix
  ];

  fonts.fontconfig.enable = true;
  home.username = "dimic";
  home.homeDirectory = "/home/dimic";
  home.file.".config/qutebrowser/config.py".source = ./dotfiles/qutebrowser/config.py;
  home.file.".dwm/autostart.sh".source = ./scripts/autostart.sh;
  home.stateVersion = "23.05";
  home.packages = with pkgs; [
    firefox
    tree
    signal-desktop
    qutebrowser
    htop
    exa
    bat
    xclip
    flameshot
    element-desktop
    scrot
    tldr
    ranger
    neofetch
    (nerdfonts.override { fonts = [ "FiraCode" ]; })
  ];
}
