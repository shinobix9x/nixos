{
  programs.git = {
    enable = true;
    userName = "ShinobiX9X";
    userEmail = "shinobix9x@pm.me";
    extraConfig = {
      init = {
        defaultBranch = "main";
      };
    };
  };
}
