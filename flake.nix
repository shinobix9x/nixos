{
  description = "NixOS System Config";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    home-manager = {
      url = github:nix-community/home-manager;
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = { nixpkgs, home-manager, ... }:
  let
    system = "x86_64-linux";
  in
  {
    nixosConfigurations = {
      NixOS-ThinkPadT550 = nixpkgs.lib.nixosSystem {
        inherit system;
        modules = [
	./nixos/configuration.nix
	home-manager.nixosModules.home-manager
	{
	  home-manager = {
	    useUserPackages = true;
	    useGlobalPkgs = true;
	    users.dimic = ./home-manager/home.nix;
	  };
	}
      ];
    };
    };
  };
}
